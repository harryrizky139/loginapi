<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\Auth;

class apiController extends Controller
{
    public function user(Request $request)
    {
        try {
            $credentials = $request->only('email', 'password');
            $user=User::where('email', $request->email)->first();
            if (Auth::attempt($credentials)) {
                return response()->json([
                    'message' => 'login sukses.',
                    'serve' => $user
                ],200); //memberikan hasil db ke pengguna
            }else{
                return response()->json([
                    'message' => $request->email." - ".$request->password,
                    'serve' => []
                ],400); //memberikan hasil db ke pengguna
            }
            
        } catch (\Exception $a) {
            return response()->json([
                'message' => 'ERROR Mamang.'.$a->getMessage(),
                'serve' => []
            ],500); //memberikan hasil db ke pengguna
        }
    }
    public function userregister(Request $request)
    {
        try {
            $pw = $request->password;
            $newpw = password_hash($pw, PASSWORD_DEFAULT);

            $dataUser = new User;
            $dataUser->name = $request->name;
            $dataUser->email = $request->email;
            $dataUser->password = $newpw;
            $dataUser->save();
            return response()->json([
                'message' => 'Buat akun sukses.',
                'serve' => $dataUser
            ],200); //memberikan hasil db ke pengguna
        } catch (\Exception $a) {
            return response()->json([
                'message' => 'ERROR Mamang.'.$a->getMessage(),
                'serve' => []
            ],500); //memberikan hasil db ke pengguna
        }
    }
}
