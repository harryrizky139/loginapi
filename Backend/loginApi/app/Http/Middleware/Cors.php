<?php

namespace App\Http\Middleware;

use Closure;

class Cors{

    public function handle($request, Closure $next){
        return $next($request)
            ->header('Access-Control-Allow-Origin','*')
            ->header('Access-Control-Allow-Method','GET, POST, PUT, DELETE, OPTION')
            ->header('Access-Control-Allow-Headers','X-Requseted-With, Content-Type, X-Token-Auth, Autorization');
    }
}
